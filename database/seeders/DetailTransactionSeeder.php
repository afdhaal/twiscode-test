<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class DetailTransactionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('detailtransaction')->insert([
            'id' => 1,
            'id_transaksi' => 1,
            'harga' => 20000,
            'jumlah' => 2,
            'subtotal' => 40000,
        ]);
    }
}
