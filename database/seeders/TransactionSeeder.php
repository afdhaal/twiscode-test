<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class TransactionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('transaction')->insert([
            'id' => 1,
            'tanggal_order' => '2021-07-21',
            'status_pelunasan' => 'lunas',
            'tanggal_pembayaran' => '2021-07-21',
        ]);
    
    }
}
