<?php

namespace App\Http\Controllers;

use App\Models\Converter;
use Illuminate\Http\Request;
use DB;

class ConverterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = DB::table('transaction')
            ->join('detailtransaction', 'transaction.id', '=', 'detailtransaction.id_transaksi')
            ->select('transaction.id','transaction.status_pelunasan','tanggal_pembayaran','subtotal as total','jumlah')
            ->get();
        return $data;
    }

    public function decimalToBinary($decimal) {
        $i = "";
        while (floor($decimal) > 0) {
            $i .= $decimal % 2;
            $decimal /= 2;
        }
        return strrev($i);
    }   

    function binaryToDecimal($binary) {
        $base=1;
        $dec_nr=0;
        $binary=explode(",", preg_replace("/(.*),/", "$1", str_replace("1", "1,", str_replace("0", "0,", $binary))));
        for($i=1; $i<count($binary); $i++) $base=$base*2;
        foreach($binary as $key=>$binary_bit) {
            if($binary_bit==1) {
                $dec_nr+=$base;
                $base=$base/2;
            }
            if($binary_bit==0) $base=$base/2;
        }
        return $dec_nr;
       }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Converter  $converter
     * @return \Illuminate\Http\Response
     */
    public function show(Converter $converter)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Converter  $converter
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Converter $converter)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Converter  $converter
     * @return \Illuminate\Http\Response
     */
    public function destroy(Converter $converter)
    {
        //
    }
}
